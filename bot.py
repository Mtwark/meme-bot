import discord
from random import randrange
from webpage import webpage
import requests
from time import sleep
import os
import imghdr

TOKEN = 'NTE1MDMzMzg1NTcyNDk5NDU3.DtfN5Q.Gv0dR0FGVe4SSELUadAxTw53OF8'
MEME_PATH = 'memes/'
COUNT_PATH = 'count.txt'
subs = ['dankmemes', 'memes', 'prequelmemes', 'blackpeopletwitter', 'whitepeopletwitter', 'wholesomememes', 'trippinthroughtime', 'raimimemes', 'lotrmemes'] # Not ready for these yet

def get_reddit_memes(subreddit):
    url = 'https://reddit.com/r/' + subreddit
    reddit = webpage(url)
    xpath = '//*[@style="background-color:#edeff1;max-height:512px;margin:0 auto"]'

    sleep(3)

    posts = reddit.get_elements(xpath)

    count = 1
    for post in posts:
        if 'external-preview' not in post.get_attribute('src'):
            response = requests.get(post.get_attribute('src'))
            if response.status_code == 200:
                with open('temp', 'wb') as temp:
                    temp.write(response.content)
                filetype = imghdr.what('temp')
                filename = MEME_PATH + 'meme' + str(count) + '.' + str(filetype)
                count += 1

                with open(filename, 'wb') as f:
                    f.write(response.content)

    reddit.close()
    return 0

client = discord.Client()

@client.event
async def on_ready():
    print("Gathering some spicy memes...")
    with open('count.txt', 'w') as f:
        f.write(str(get_reddit_memes('dankmemes')))
    await client.change_presence(game=discord.Game(name="Memeing"))
    print("The bot is ready")


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content[0] != '!':
        return
    if ("quit" or "leave" or "exit") in message.content:
        exits = ["Lata", "Peace", "Deuces", "See ya", "So it's gonna be like that...", "I got better things to do anyways."]
        await client.send_message(message.channel, exits[randrange(len(exits))])
        exit()
    if " " in message.content:
        mess = message.content.split()
    else:
        mess = message.content
    if ("meme" or "Meme") in mess:
        memes = os.listdir(MEME_PATH)
        meme = MEME_PATH + str(memes[randrange(len(memes))])
        subreddit = 'dankmemes'
        print("Getting meme")

        await client.send_file(message.channel, meme)
        if os.path.exists(meme):
            os.remove(meme)
        print("Memed")
        if len(os.listdir(MEME_PATH)) == 0:
            get_reddit_memes(subreddit)
    else:
        await client.send_message(message.channel, "Bruh, I don't know how to do that.")

client.run(TOKEN)