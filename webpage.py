from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from time import sleep


# Class that functions on top of seleniums webdriver and assumes xpath is used for functions
class webpage:
    # Initialize webpage, starting chrome driver and loading url
    def __init__(self, url):

        # Chromedriver settings
        CHROME_PATH = "/opt/google/chrome/google-chrome"
        CHROMEDRIVER_PATH = './chromedriver'
        WINDOW_SIZE = "1920,1080"
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--window-size=%s" % WINDOW_SIZE)
        chrome_options.binary_location = CHROME_PATH
        self.browser = webdriver.Chrome(executable_path=CHROMEDRIVER_PATH,
                                        chrome_options=chrome_options)
        self.url = url
        self.browser.get(self.url)
        self.timeout = 30

    # Checks if an element at given xpath is found
    def check_for_element(self, xpath):
        try:
            self.browser.find_element_by_xpath(xpath)
            return True
        except:
            return False

    # Waits for timeout seconds for an element to appear, then throws NoSuchElement
    def wait_for_element(self, xpath, timeout):
        counter = 0
        while not self.check_for_element(xpath) and counter < timeout:
            counter += 1
            sleep(1)

    # Waits for at least one element at given xpath to exist or for timeout ot be hit
    def wait_for_elements(self, xpath, timeout):
        counter = 0
        test = self.browser.find_elements_by_xpath(xpath)
        while len(test) < 1 and counter < timeout:
            counter += 1
            test = self.browser.find_elements_by_xpath(xpath)
            sleep(1)

    # Waits for elements to exist at xpath, then returns them as a list
    def get_elements(self, xpath):
        self.wait_for_elements(xpath, self.timeout)
        elements = self.browser.find_elements_by_xpath(xpath)
        return elements

    # Opens webpage in current browser
    def open(self, url):
        self.browser.get(url)

    # Closes the webpage
    def close(self):
        self.browser.close()

    # Click on the element pointed to by the input xpath
    def click(self, xpath):
        self.wait_for_element(xpath, self.timeout)
        element = self.browser.find_element_by_xpath(xpath)
        element.click()

    # Pass any element here and it will scroll to bottom, loading more content
    def scroll_bottom(self):
        elm = self.browser.find_element_by_tag_name('html')
        elm.send_keys(Keys.END)


    # Enters text into a text field
    def text_field(self, xpath, text):
        self.wait_for_element(xpath, self.timeout)
        text = str(text)
        webdriver.ActionChains(self.browser). \
            move_to_element(self.browser.find_element_by_xpath(xpath)). \
            click(). \
            send_keys(text). \
            perform()

    # Find username and password fields and populate them with information from input file.
    def login(self, login_path, user_xpath, pass_xpath, button_xpath):
        print("Getting login info...")

        with open(login_path, "r") as login_file:
            username = login_file.readline()[9:-1]
            password = login_file.readline()[9:]
        self.wait_for_element(user_xpath, self.timeout)

        print("Logging in")
        self.text_field(user_xpath, username)
        if not self.check_for_element(pass_xpath):
            self.click(button_xpath)
            self.text_field(pass_xpath, password)
            self.browser.find_element_by_xpath(pass_xpath).send_keys(
                u'\ue007')  # Press enter to continue since login button may be different
        else:
            self.text_field(pass_xpath, password)
            self.click(button_xpath)

        print("Logged in as", username)